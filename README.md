# Simple chat (PHP , Ratchet - WebSockets , HTML,CSS,JS)

Le projet "Chat" est un système de chat simple en temps réel développé en utilisant PHP, Ratchet - WebSockets, HTML, CSS et JavaScript. Ce projet permet aux utilisateurs de communiquer en temps réel en utilisant un navigateur web. Le système de chat utilise Ratchet - WebSockets pour établir une connexion en temps réel entre le navigateur web de l'utilisateur et le serveur, permettant ainsi l'échange de messages en temps réel. Le système de chat utilise également PHP pour traiter les requêtes du navigateur et stocker les messages . L'interface utilisateur du chat est développée en utilisant HTML, CSS et JavaScript, permettant une expérience utilisateur intuitive.

    Pour démarrez le serveur WS - " php server/server.php "
    ...


