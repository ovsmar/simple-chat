<?php

// Déclaration d'un espace de noms
namespace MyApp;

// on appelle la classe MessageComponentInterfac de Ratchet
use Ratchet\MessageComponentInterface;
// on appelle la classe ConnectionInterface de Ratchet
use Ratchet\ConnectionInterface;


// classe pour gérer le chat
class Chat implements MessageComponentInterface {
    // Propriété pour stocker tous les utilisateurs connectés
    protected $clients;
   
    // constructeur de la classe
    public function __construct() {
        // stockage
        $this->clients = new \SplObjectStorage;
    }
   
    // Méthode pour ouvrir une nouvelle connexion
    public function onOpen(ConnectionInterface $conn) {
        //$ pour ajouter un nouvel utilisateur
        $this->clients->attach($conn);
        // on fait echo pour afficher connexion d'un nouvel utilisateur
        echo "Nouvelle connexion ({$conn->resourceId})\n";
        
    }
    

     // Méthode pour recevoir et envoyer les message
    public function onMessage(ConnectionInterface $from, $msg) {
        // on fait echo pour afficher que le message a été reçu
        echo "Réception du message: " . $msg;
        
        
        // on boucle sur tous les utilisateurs
        foreach ($this->clients as $client) {
            // Envoi d'un message à l'utilisateur
            $client->send($msg);
        }
    }
    
    // Méthode pour déconnecter un utilisateur
    public function onClose(ConnectionInterface $conn) {
        // pour déconnecter le client
        $this->clients->detach($conn);
        
        // on fait echo pour afficher que l'utilisateur ete déconnecter
        echo "Déconnection d'un utilisateur {$conn->resourceId} \n";
    }
    
    // Méthode pour traiter les errors
    public function onError(ConnectionInterface $conn, \Exception $e) {
        // on fait echo pour afficher les errors
        echo "error!: {$e->getMessage()}\n";
       
        // Désactiver de la connexion 
        $conn->close();
    }
}