var chat = {
    // recup HTML ELEMENTS
    host : "ws://localhost:8080/", 
    name : "", 
    socket : null, 
    Msg : null, 
    Name : null, 
    NameSet : null, 
    NameGo : null, 
    Send : null, 
    SendTxt : null,
    SendGo : null, 

    getAllElements : () => {
      chat.Msg = document.getElementById("chat-messages");
      chat.Name = document.getElementById("chat-name");
      chat.NameSet = document.getElementById("chat-name-set");
      chat.NameGo = document.getElementById("chat-name-go");
      chat.Send = document.getElementById("chat-send");
      chat.SendTxt = document.getElementById("chat-send-text");
      chat.SendGo = document.getElementById("chat-send-go");
      chat.NameGo.disabled = false;
    },
   
    // Échanger entre name/send form
    setnameTOsend : direction => {
      if (direction) {
        chat.Name.classList.add("hide");
        chat.Send.classList.remove("hide");
      } else {
        chat.Send.classList.add("hide");
        chat.Name.classList.remove("hide");
      }
    },
   
    start : () => {
      chat.name = chat.NameSet.value;
      chat.setnameTOsend(1);
      chat.SendGo.disabled = true;
   
      // WEB SOCKET
      chat.socket = new WebSocket(chat.host);
   
      // activer chat
      chat.socket.onopen = e => chat.SendGo.disabled = false;
   
      // quand chat est désactiver retourner sur choisir le nom
      chat.socket.onclose = e => chat.setnameTOsend(0);
   
      // name et messages
      chat.socket.onmessage = e => {
        let msg = JSON.parse(e.data),
        row = document.createElement("div");
        row.className = "chat-row";
        row.innerHTML = `<div class="chat-name">${msg.n}</div><div class="chat-msg">${msg.m}</div>`;
        chat.Msg.appendChild(row);
      };
   
      // afficher error
      chat.socket.onerror = e => {
        chat.setnameTOsend(0);
        console.error(e);
        alert(`Impossible de se connecter à ${chat.host}`);
      };
   
      return false;
    },
  
    // pour envoyer les messages
    send : () => {
      chat.socket.send(JSON.stringify({
        n: chat.name,
        m: chat.SendTxt.value
      }));
      chat.SendTxt.value = "";
      return false;
    }
  };
  window.onload = chat.getAllElements;